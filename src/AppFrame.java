
import java.awt.*;
import javax.swing.*;
import javaprojectexamples.CounterPanel;

/**
 * Hello world!
 *
 */
public class AppFrame extends JFrame {

    JPanel panel;
    
    public AppFrame() {
        initPanel();
        this.setDefaultBehaviour();

    }

    private void initPanel() {
        panel = new CounterPanel();
        this.setContentPane(panel);
    }

    private void setDefaultBehaviour() { 

        this.setMinimumSize(new Dimension(200, 200));
        this.setMaximumSize(new Dimension(500, 500));

        this.setSize(new Dimension(300, 300));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.setLocation(
         (screenSize.width) / 4,
         (screenSize.height) / 4
        );
        
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.pack();
        this.setVisible(true);
    }

}
