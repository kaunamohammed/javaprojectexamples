package javaprojectexamples;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;

public class CounterPanel extends JPanel {

    private int count = 0;

    private static String COUNTER_LABEL_TEXT = "Count: ";

    JButton incrButton;
    JButton decrButton;

    JLabel counterLabel;
    
    public CounterPanel() {
        super();
        this.setLayout(new GridLayout());
        this.setBorder(BorderFactory.createEmptyBorder());

        incrButton = new JButton("Increment");
        incrButton.addActionListener(this::incrButtonTapped);
        this.add(incrButton);

        counterLabel = new JLabel(COUNTER_LABEL_TEXT+ count, SwingConstants.CENTER);
        this.add(counterLabel);
        
        decrButton = new JButton("Decrement");
        decrButton.addActionListener(this::decrButtonTapped);
        this.add(decrButton);

    }

    private void incrButtonTapped(ActionEvent e) {
        count++;
        counterLabel.setText(COUNTER_LABEL_TEXT + count);
    }

    private void decrButtonTapped(ActionEvent e) {
        count--;

        if (count < 0) {
            count = 0;
            return;
        }
        counterLabel.setText(COUNTER_LABEL_TEXT + count);
    }

}